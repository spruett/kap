use std::fmt::Debug;
use std::result;
use std::sync::{Arc, Mutex};

use codemap::CodeMap;
use codemap_diagnostic::Diagnostic;
use failure;

pub type Result<T> = result::Result<T, failure::Error>;

/*
 * Error-handling pattern from the `failure` crate, see
 * https://boats.gitlab.io/failure/guidance.html
 */

#[derive(failure::Fail)]
#[fail(display = "Starlark eval error")]
pub struct EvaluationError {
    pub diagnostic: Diagnostic,
    pub codemap: Arc<Mutex<CodeMap>>,
}

impl Debug for EvaluationError {
    fn fmt(&self, formatter: &mut std::fmt::Formatter) -> result::Result<(), std::fmt::Error> {
        write!(
            formatter,
            "diagnostic: {:?}",
            self.diagnostic,
        )
    }
}
