#!/usr/bin/env cargo run -- eval

NAME = 'kap'
VERSION = '0.1.1'
ABOUT = '''A better way of applying Kubernetes configuration'''


def make_subcommand(
    name,
    about,
    aliases=[],
    args=[],
):
    return {
        name: {
            'about': about,
            'aliases': aliases,
            'args': args,
        }
    }


def make_arg(
    name,
    **kwargs,
):
    return {name: kwargs}

spec_file_arg = make_arg('file', help='Path to a spec file (.star) to evaluate', index=1, required=True)

subcommands = [
    make_subcommand(
        'eval',
        aliases=['e'],
        about='Evaluate a spec (.star) file and print the corresponding output (YAML) without applying it',
        args=[spec_file_arg]
    ),
    make_subcommand(
        'list',
        aliases=['l', 'ls'],
        about='Evaluate a spec (.star) file and list all Kubernetes objects (kind and name)',
        args=[spec_file_arg]
    ),
    make_subcommand(
        'diff',
        about='Evaluate a spec (.star) file and diff the result with running resources via `kubectl diff`',
        args=[spec_file_arg],
    ),
    make_subcommand(
        'apply',
        aliases=['a', 'ap'],
        about='Evaluate a spec (.star) file and apply the results via `kubectl apply`',
        args=[spec_file_arg],
    ),
]

{
    'name': NAME,
    'version': VERSION,
    'about': ABOUT,

    'settings': ['ArgRequiredElseHelp'],
    'subcommands': subcommands,
}