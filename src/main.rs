mod error;
mod eval;
mod kubectl;
mod loader;
mod serialization;

use std::path::Path;

use clap;
use starlark::values::Value;

fn abort_on_error<T>(result: error::Result<T>) -> T {
    match result {
        Ok(val) => val,
        Err(err) => {
            eval::print_error(err);
            std::process::exit(1);
        }
    }
}

fn get_item_or_abort(value: &Value, name: &str) -> Value {
    match value.at(Value::new(name.to_owned())) {
        Ok(res) => res,
        Err(err) => {
            eprintln!("{:?}", err);
            std::process::exit(1);
        }
    }
}

fn eval_specs_from_matches(matches: &clap::ArgMatches) -> Vec<Value> {
    let filename = matches.value_of("file").unwrap();
    let val = abort_on_error(eval::evaluate_file(Path::new(&filename)));
    eval::value_to_spec_list(val)
}

fn eval_yaml_from_matches(matches: &clap::ArgMatches) -> String {
    let specs = eval_specs_from_matches(matches);
    abort_on_error(serialization::values_to_yaml(&specs))
}

fn eval_command(matches: &clap::ArgMatches) {
    print!("{}", eval_yaml_from_matches(matches));
}

fn apply_command(matches: &clap::ArgMatches) {
    let yml = eval_yaml_from_matches(matches);
    abort_on_error(kubectl::kubectl_with_input(
        vec!["apply", "-f", "-"],
        yml,
    ));
}

fn diff_command(matches: &clap::ArgMatches) {
    let yml = eval_yaml_from_matches(matches);
    abort_on_error(kubectl::kubectl_with_input(
        vec!["diff", "-f", "-"],
        yml,
    ));
}

fn list_command(matches: &clap::ArgMatches) {
    let specs = eval_specs_from_matches(matches);
    for spec in specs {
        let kind = get_item_or_abort(&spec, "kind");
        let metadata = get_item_or_abort(&spec, "metadata");
        let name = get_item_or_abort(&metadata, "name");

        println!("{}/{}", kind.to_str().to_ascii_lowercase(), name.to_str());
    }
}

fn main() {
    let app_yml = clap::load_yaml!("gen/clap.yml");
    let matches = clap::App::from_yaml(app_yml).get_matches();

    if let Some(eval_matches) = matches.subcommand_matches("eval") {
        eval_command(eval_matches)
    } else if let Some(apply_matches) = matches.subcommand_matches("apply") {
        apply_command(apply_matches)
    } else if let Some(diff_matches) = matches.subcommand_matches("diff") {
        diff_command(diff_matches)
    } else if let Some(list_matches) = matches.subcommand_matches("list") {
        list_command(list_matches);
    }
}
