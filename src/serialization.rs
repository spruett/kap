use std::result::Result;

use starlark::values::Value;
use serde::{Serialize, Serializer};
use serde::ser::{SerializeMap, SerializeSeq};

use crate::error;

struct SerializableValue<'a> {
    value: &'a Value
}

impl Serialize for SerializableValue<'_> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error> 
    where 
        S: Serializer
    {
        match self.value.get_type() {
            "string" => serializer.serialize_str(self.value.to_str().as_ref()),
            "int" => serializer.serialize_i64(self.value.to_int().unwrap()),
            "bool" => serializer.serialize_bool(self.value.to_bool()),
            "dict" => {
                let mut map = serializer.serialize_map(None)?;
                let cloned = self.value.clone();
                for key in cloned.into_iter().unwrap() {
                    let mapped = self.value.at(key.clone()).unwrap();
                    map.serialize_key(&SerializableValue { value: &key })?;
                    map.serialize_value(&SerializableValue { value: &mapped })?;
                }
                map.end()
            }
            "tuple" | "list" => {
                let mut seq = serializer.serialize_seq(None)?;
                for item in self.value.into_iter().unwrap() {
                    seq.serialize_element(&SerializableValue { value: &item })?;
                }
                seq.end()
            }
            "NoneType" => serializer.serialize_none(),
            _ => panic!("unserializable type: {}", self.value.get_type()),
        }
    }
}

pub fn values_to_yaml(values: &[Value]) -> error::Result<String> {
    let mut buffer = String::new();
    for value in values {
        buffer += &serde_yaml::to_string(&SerializableValue{ value })?;
        buffer += "\n";
    }
    Ok(buffer)
}