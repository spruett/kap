use std::collections::HashMap;
use std::sync::{Arc, Mutex};
use std::path::{Path, PathBuf};
use std::fs::File;
use std::convert::From;
use std::io::Read;

use codemap::CodeMap;
use codemap_diagnostic::{Diagnostic, Level};
use failure::format_err;
use starlark::environment::Environment;
use starlark::eval::{EvalException, FileLoader};

use crate::error;

#[derive(Clone)]
pub struct ModuleLoader {
    preloaded_libs: HashMap<String, String>,
    lib_dirs: Vec<PathBuf>,

    environment: Environment,
    codemap: Arc<Mutex<CodeMap>>,
    loaded_libs: Arc<Mutex<HashMap<String, Environment>>>,
}

pub fn read_file(path: &Path) -> error::Result<String> {
    let mut buf = String::new();
    match File::open(path).map(|mut f| f.read_to_string(&mut buf)) {
        Ok(_) => Ok(buf),
        Err(e) => {
            let err_msg = format!("failed to open file ({}): {}", path.to_string_lossy(), e);
            let error: failure::Error = From::from(e);
            Err(From::from(error.context(err_msg)))
        }
    }
}


impl ModuleLoader {
    pub fn new(
        preloaded_libs: HashMap<String, String>,
        lib_dirs: Vec<PathBuf>,
        environment: Environment,
        codemap: Arc<Mutex<CodeMap>>,
    ) -> ModuleLoader {
        ModuleLoader {
            preloaded_libs,
            lib_dirs,
            environment,
            codemap,
            loaded_libs: Arc::new(Mutex::new(HashMap::new())),
        }
    }

    fn get_cached(&self, path: &str) -> Option<Environment> {
        let locked_libs = self.loaded_libs.lock().unwrap();
        locked_libs.get(path).cloned()
    }

    fn get_module_content(&self, path: &str) -> error::Result<String> {
        if path.starts_with("//") {
            if let Some(preloaded) = self.preloaded_libs.get(path) {
                return Ok(preloaded.clone())
            }

            let path_suffix = &path[2..];
            for dir in &self.lib_dirs {
                let filename = dir.join(path_suffix);
                if filename.exists() {
                    return read_file(&filename);
                }
            }
            return Err(format_err!("no library module '{}' found", path));
        }

        read_file(Path::new(path))
    }
}

impl FileLoader for ModuleLoader {
    fn load(&self, path: &str) -> Result<Environment, EvalException> {
        if let Some(cached) = self.get_cached(path) {
            return Ok(cached);
        }

        let content = self.get_module_content(path)
            .map_err(|e| {
                let diagnostic = Diagnostic {
                    level: Level::Error,
                    message: format!("in module '{}': {}", self.environment.name(), e),
                    spans: vec![],
                    code: Some("IO".to_owned()),
                };
                EvalException::DiagnosedError(diagnostic)
            })?;
        let mut environment = self.environment.child(path);
        if let Err(d) = starlark::eval::eval(
            &self.codemap,
            path,
            &content,
            false,
            &mut environment,
            self.clone(),
        ) {
            return Err(EvalException::DiagnosedError(d));
        }
        environment.freeze();
        self.loaded_libs
            .lock()
            .unwrap()
            .insert(path.to_owned(), environment.clone());
        Ok(environment)
    }
}
