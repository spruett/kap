use std::collections::HashMap;
use std::path::Path;
use std::sync::{Arc, Mutex};

use codemap::CodeMap;
use codemap_diagnostic::{ColorConfig, Emitter};
use failure::Error;
use starlark::environment::Environment;
use starlark::values::Value;

use crate::error;
use crate::loader::{read_file, ModuleLoader};

fn make_codemap() -> Arc<Mutex<CodeMap>> {
    Arc::new(Mutex::new(CodeMap::new()))
}

fn setup_environment(name: &str) -> Environment {
    starlark::stdlib::global_environment().child(name)
}

fn load_std_libs() -> HashMap<String, String> {
    let mut libs = HashMap::new();
    libs.insert(
        String::from("//kap/configmap.star"),
        include_str!("kaplib/configmap.star").to_owned(),
    );
    libs.insert(
        String::from("//kap/deployment.star"),
        include_str!("kaplib/deployment.star").to_owned(),
    );
    libs.insert(
        String::from("//kap/ingress.star"),
        include_str!("kaplib/ingress.star").to_owned(),
    );
    libs.insert(
        String::from("//kap/pod.star"),
        include_str!("kaplib/pod.star").to_owned(),
    );
    libs.insert(
        String::from("//kap/service.star"),
        include_str!("kaplib/service.star").to_owned(),
    );
    libs
}

pub fn value_to_spec_list(value: Value) -> Vec<Value> {
    if value.get_type() == "list" || value.get_type() == "tuple" {
        return value.into_iter().unwrap().collect();
    }
    return vec![value];
}

pub fn evaluate_file(path: &Path) -> error::Result<Value> {
    let codemap = make_codemap();
    let mut env = setup_environment(&path.to_str().unwrap());
    let contents = read_file(path)?;

    let loader = ModuleLoader::new(load_std_libs(), vec![], env.clone(), codemap.clone());

    let result = starlark::eval::eval(
        &codemap,
        path.to_str().unwrap(),
        &contents,
        /* build */ false,
        &mut env,
        loader.clone(),
    );

    let val = result.map_err(|diagnostic| error::EvaluationError {
        diagnostic,
        codemap,
    })?;
    Ok(val)
}

pub fn print_error(err: Error) {
    match err.downcast::<error::EvaluationError>() {
        Ok(eval_err) => {
            let codemap = eval_err.codemap.lock().unwrap();
            let mut emitter = Emitter::stderr(ColorConfig::Auto, Some(&codemap));
            emitter.emit(&[eval_err.diagnostic]);
        }
        Err(other_err) => eprintln!("{}", other_err),
    }
}
