use std::process::{Command, Stdio};
use std::io::Write;

use failure::format_err;

use crate::error;

pub fn kubectl_with_input(args: Vec<&str>, input: String) -> error::Result<()> {
    let mut child = Command::new("kubectl")
        .args(args)
        .stdin(Stdio::piped())
        .spawn()?;


    if let Some(stdin) = &mut child.stdin {
        write!(stdin, "{}", input)?;
    }

    let status = child.wait()?;
    if status.success() {
        Ok(())
    } else {
        Err(format_err!("kubectl failed: {}", status))
    }
}