def make_configmap(
    name,
    namespace=None,
    data={},
):
    return {
        'apiVersion': 'v1',
        'kind': 'ConfigMap',
        'metadata': {
            'name': name,
            'namespace': namespace,
        },
        'data': data,
    }