def make_pod(name, namespace=None, containers=[]):
    return {
        'apiVersion': 'v1',
        'kind': 'Pod',
        'metadata': {
            'name': name,
            'namespace': namespace,
        },
        'spec': {
            'containers': containers,
        }
    }


def make_container(image, name=None, **kwargs):
    name = image.split('/')[-1].split(':')[0]
    container = {
        'name': name,
        'image': image,
    }
    container.update(kwargs)
    return container