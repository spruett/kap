def make_port(
    port,
    protocol="TCP",
    target_port=None,
):
    return {
        'port': port,
        'protocol': protocol,
        'targetPort': target_port or port,
    }

def make_service(
    name,
    namespace=None,
    ports=[],
    selector=None,
    app=None,
    labels={},
    **kwargs
):
    if selector == None and app != None:
        selector = {
            'app': app,
        }
    if app != None and not 'app' in labels:
        labels['app'] = app
    service = {
        'apiVersion': 'v1',
        'kind': 'Service',
        'metadata': {
            'name': name,
            'namespace': namespace,
            'labels': labels,
        },
        'spec': {
            'selector': selector,
            'ports': ports,
        }
    }
    service.update(kwargs)
    return service