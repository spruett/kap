def make_ingress_http_rule(
    host=None,
    backend_service=None,
    backend_service_port=None,
    path="/",
):
    return {
        'host': host,
        'http': {
            'paths': [
                {
                    'backend': {
                        'serviceName': backend_service,
                        'servicePort': backend_service_port,
                    },
                    'path': path,
                }
            ]
        }
    }

def make_ingress(
    name,
    namespace=None,
    labels={},
    app=None,
    annotations={},
    rules=[],
    tls_hosts=[],
    tls_secret_name=None,
    **kwargs
):
    tls = []
    if tls_hosts:
        tls_secret_name = tls_secret_name or (name + '-tls')
        tls = [
            {
                'hosts': tls_hosts,
                'secretName': tls_secret_name,
            }
        ]
    if app != None and not 'app' in labels:
        labels['app'] = app
    ingress = {
        'apiVersion': 'extensions/v1beta1',
        'kind': 'Ingress',
        'metadata': {
            'name': name,
            'annotations': annotations,
            'labels': labels,
        },
        'spec': {
            'rules': rules,
            'tls': tls,
        }
    }
    ingress.update(kwargs)
    return ingress