def make_deployment(
    name,
    namespace=None,
    app=None,
    labels={},
    selector_labels={},
    replicas=1,
    containers=[],
    pod_spec={},
    **kwargs
):
    app = app or name
    if app != None and not 'app' in labels:
        labels['app'] = app
    template = {
        'metadata': {
            'labels': labels,
        },
        'spec': {
            'containers': containers,
        }
    }
    if not selector_labels and app != None:
        selector_labels['app'] = app

    template['spec'].update(pod_spec)
    deployment = {
        'apiVersion': 'apps/v1',
        'kind': 'Deployment',
        'metadata': {
            'namespace': namespace,
            'name': name,
            'labels': labels,
        },
        'spec': {
            'selector': {
                'matchLabels': selector_labels,
            },
            'replicas': replicas,
            'template': template,
        }
    }
    deployment.update(kwargs)
    return deployment
