load('//kap/pod.star', 'make_pod', 'make_container')

[
    make_pod(
        name="test-pod",
        namespace="test",
        containers=[
            make_container('busybox', command="echo hi".split())
        ]
    ),
    make_pod(
        name="test-pod2",
        containers=[
            make_container('busybox', command="echo ho".split())
        ]
    ),
]